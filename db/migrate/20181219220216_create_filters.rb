class CreateFilters < ActiveRecord::Migration[5.2]
  def change
    create_table :filters do |t|
      t.belongs_to :city
      t.date :from, default: nil
      t.date :to,   default: nil

      t.timestamps
    end

    create_table :filters_themes do |t|
      t.belongs_to :theme,  index: true
      t.belongs_to :filter, index: true
    end
  end
end
