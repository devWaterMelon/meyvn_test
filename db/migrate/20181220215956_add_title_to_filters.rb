class AddTitleToFilters < ActiveRecord::Migration[5.2]
  def change
    add_column :filters, :title, :string
  end
end
