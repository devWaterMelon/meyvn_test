class CreateCitiesEventsThemes < ActiveRecord::Migration[5.2]
  def change
    create_table :cities do |t|
      t.string :title

      t.timestamps
    end


    create_table :events do |t|
      t.string :title
      t.date :from, null: false
      t.date :to, null: false
      t.belongs_to :city

      t.timestamps
    end


    create_table :themes do |t|
      t.string :title

      t.timestamps
    end


    create_table :events_themes do |t|
      t.belongs_to :event, index: true
      t.belongs_to :theme, index: true
    end
  end
end
