# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_20_215956) do

  create_table "cities", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", force: :cascade do |t|
    t.string "title"
    t.date "from", null: false
    t.date "to", null: false
    t.integer "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_events_on_city_id"
  end

  create_table "events_themes", force: :cascade do |t|
    t.integer "event_id"
    t.integer "theme_id"
    t.index ["event_id"], name: "index_events_themes_on_event_id"
    t.index ["theme_id"], name: "index_events_themes_on_theme_id"
  end

  create_table "filters", force: :cascade do |t|
    t.integer "city_id"
    t.date "from"
    t.date "to"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
    t.index ["city_id"], name: "index_filters_on_city_id"
  end

  create_table "filters_themes", force: :cascade do |t|
    t.integer "theme_id"
    t.integer "filter_id"
    t.index ["filter_id"], name: "index_filters_themes_on_filter_id"
    t.index ["theme_id"], name: "index_filters_themes_on_theme_id"
  end

  create_table "themes", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
