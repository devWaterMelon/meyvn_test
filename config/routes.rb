Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :admin do
    resources :cities
    resources :themes
    resources :events
    resources :filters

    root to: 'events#index'
  end

  root to: 'events#show'

  resource :events, only: [:show] do
    get 'filter', to: 'events#filter'
    post 'filter/save', to: 'events#save_filter', as: :save_filter
  end

  get 'filter/delete/:id', to: 'filter#delete'

end
