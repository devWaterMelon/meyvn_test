class Event < ApplicationRecord
  belongs_to :city
  has_and_belongs_to_many :themes

  paginates_per 10

  scope :with_relations, -> { includes(:city, :themes) }
  scope :by_city, ->(city_id) { where(city_id: city_id) }
  scope :by_themes, ->(theme_ids) { joins(:themes).merge(Theme.where(id: theme_ids)) }

  scope :get_from_date, ->(from) { where("`from` >= :date", date: from.to_date) }
  scope :get_to_date,   ->(to)   { where("`to` <= :date",   date: to.to_date  ) }
end
