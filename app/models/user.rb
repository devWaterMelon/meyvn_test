class User < ApplicationRecord

  self.abstract_class = true

  def self.current
    'user'
  end

  def self.queue_name
    "#{current}_list"
  end

  def self.online?
    Rails.cache.redis.get(current)
  end

  def self.set_online
    Rails.cache.redis.set(current, true)
  end

  def self.set_offline
    Rails.cache.redis.del(current)
  end

  def self.add_to_queue(value)
    Rails.cache.redis.rpush(queue_name, value)
  end

  def self.broadcast(data)
    EventChannel.broadcast_to User.current, event: data
  end

  def self.notify_from_offline
    count = Rails.cache.redis.llen(queue_name)

    (1..count).each do
      data = Rails.cache.redis.lpop(queue_name)
      broadcast data
    end
  end

end
