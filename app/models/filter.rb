class Filter < ApplicationRecord
  belongs_to :city, optional: true
  has_and_belongs_to_many :themes
end
