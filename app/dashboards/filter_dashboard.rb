require "administrate/base_dashboard"

class FilterDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    city: Field::BelongsTo,
    themes: Field::HasMany,
    id: Field::Number,
    from: Field::DateTime,
    to: Field::DateTime,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    title: Field::String,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :title,
    :city,
    :themes,
    :id,
    :from,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :title,
    :city,
    :themes,
    :id,
    :from,
    :to,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :title,
    :city,
    :themes,
    :from,
    :to,
  ].freeze

  # Overwrite this method to customize how filters are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(filter)
    filter.title
  end
end
