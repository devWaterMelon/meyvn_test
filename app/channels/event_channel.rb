class EventChannel < ApplicationCable::Channel
  def subscribed
    stream_from "event:#{User.current}"
    User.set_online
    User.notify_from_offline
  end

  def unsubscribed
    User.set_offline
  end

end
