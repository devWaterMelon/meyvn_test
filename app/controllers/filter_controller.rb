class FilterController < ApplicationController

  rescue_from Exception, with: :json_error

  def delete
    Filter.delete(params[:id])
    render json: { success: true }
  end

end
