class CityController < ApplicationController

  def list
    @city_list = City.all
  end

  def check
    if params[:city_id].to_i.zero?
      cookies.delete :city_id
    else
      cookies[:city_id] = { value: params[:city_id], expires: Time.now + 1.year }
    end
  end

end
