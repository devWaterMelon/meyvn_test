class EventsController < ApplicationController

  helper FilterHelper

  def show
    @events = Event.all.page(params[:page])
    create_filter
    filter_events
  end

  def save_filter
    create_filter

    begin
      @filter.save!
    rescue
      return render json: { success: false }
    end

    render json: { success: true }
  end


  private

  def filter_params
    params.permit(filter: [:title, :city_id, :from, :to,])[:filter]
  end

  def theme_ids_params
    params.permit(themes: [ids: []]).fetch(:themes, {}).fetch(:ids, [])
  end

  def create_filter
    @filter = Filter.new(filter_params)
    @filter.themes << Theme.find(theme_ids_params)
  end

  def filter_events
    filter_by_city
    filter_by_theme
    filter_by_range
  end

  def filter_by_city
    @events = @events.by_city(@filter.city_id) if @filter.city_id&.positive?
  end

  def filter_by_theme
    if @filter.themes && !@filter.themes.empty?
      @events = @events.by_themes(@filter.themes.map(&:id))
    end
  end

  def filter_by_range
    @events = @events.get_from_date(@filter.from) if @filter.from
    @events = @events.get_to_date(@filter.to)     if @filter.to
  end

end
