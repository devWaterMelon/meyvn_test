class ApplicationController < ActionController::Base

  def json_error
    render json: { success: false }
  end

end
