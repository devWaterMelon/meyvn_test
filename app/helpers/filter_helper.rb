module FilterHelper

  def filter_url(filter)
    @filter = filter
    link_to filter.title.to_s, events_path(params: filter_params)
  end

  def filter_params
    {
      themes: {
        ids: @filter.themes.map(&:id),
      },
      filter: {
        city_id: @filter.city_id,
        from: @filter.from,
        to: @filter.to,
      }.delete_if { |k, _| k.nil? }
    }
  end

end
